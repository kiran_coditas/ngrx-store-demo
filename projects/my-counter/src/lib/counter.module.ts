import { NgModule, Injector } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { CounterComponent } from "./counter.component";
import { CounterIncrementComponent } from "./counter-increment/counter-increment.component";
import { CounterDecrementComponent } from "./counter-decrement/counter-decrement.component";
import { CounterResetComponent } from "./counter-reset/counter-reset.component";
import { StoreModule } from "@ngrx/store";
import { counterReducer } from "./counter.reducer";
import { userReducer } from "./user.reducer";
import { createCustomElement } from "@angular/elements";
import { MyltiplyComponent } from "./myltiply/myltiply.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";

@NgModule({
  declarations: [
    CounterComponent,
    CounterIncrementComponent,
    CounterDecrementComponent,
    CounterResetComponent,
    MyltiplyComponent,
    UserProfileComponent
  ],
  imports: [
    CommonModule,
    MatButtonModule,
    MatCardModule,
    StoreModule.forRoot({ count: counterReducer, count1: userReducer })
    // count: counterReducer,
  ],
  entryComponents: [
    CounterComponent,
    CounterIncrementComponent,
    CounterDecrementComponent,
    CounterResetComponent,
    MyltiplyComponent,
    UserProfileComponent
  ]
})
export class CounterModule {
  constructor(private injector: Injector) {
    const CounterElement = createCustomElement(CounterComponent, { injector });
    // Register the custom element with the browser.
    customElements.define("counter-element", CounterElement);

    const CounterIncrementElement = createCustomElement(
      CounterIncrementComponent,
      { injector }
    );
    customElements.define("counter-increment", CounterIncrementElement);
    const CounterDecrementElement = createCustomElement(
      CounterDecrementComponent,
      { injector }
    );
    customElements.define("counter-decrement", CounterDecrementElement);
    const CounterResetElement = createCustomElement(CounterResetComponent, {
      injector
    });
    customElements.define("counter-reset", CounterResetElement);
    const CounterMultiplyElement = createCustomElement(MyltiplyComponent, {
      injector
    });
    customElements.define("counter-multiply", CounterMultiplyElement);
    const UserProfile = createCustomElement(UserProfileComponent, {
      injector
    });
    customElements.define("user-profile", UserProfile);
  }
}
