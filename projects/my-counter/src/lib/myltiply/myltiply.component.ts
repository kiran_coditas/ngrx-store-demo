import { Component, OnInit } from "@angular/core";
import { Multiplycount } from "../counter.actions";
import { Store } from "@ngrx/store";
@Component({
  selector: "lib-myltiply",
  templateUrl: "./myltiply.component.html",
  styleUrls: ["./myltiply.component.css"]
})
export class MyltiplyComponent implements OnInit {
  constructor(private store: Store<{ count: number }>) {}

  ngOnInit() {}
  Multiply() {
    this.store.dispatch(new Multiplycount());
  }
}
