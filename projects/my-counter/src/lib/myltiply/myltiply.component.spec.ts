import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MyltiplyComponent } from './myltiply.component';

describe('MyltiplyComponent', () => {
  let component: MyltiplyComponent;
  let fixture: ComponentFixture<MyltiplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyltiplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MyltiplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
