import { Component, OnInit } from "@angular/core";
import { Store, select } from "@ngrx/store";
import {
  addUser,
  NewUser,
  UpdateUser,
  Delete,
  viewUser
} from "../user.actions";
import { Observable } from "rxjs";

@Component({
  selector: "lib-user-profile",
  templateUrl: "./user-profile.component.html",
  styleUrls: ["./user-profile.component.css"]
})
export class UserProfileComponent implements OnInit {
  constructor(private store: Store<{ count1: any }>) {}
  count1$: Observable<any[]>;
  ngOnInit() {
    this.count1$ = this.store.pipe(select("count1"));
  }
  addUser() {
    this.store.dispatch(new addUser());
  }
  NewUser() {
    this.store.dispatch(new NewUser());
  }
  UpdateUser() {
    this.store.dispatch(new UpdateUser());
  }
  Delete() {
    this.store.dispatch(new Delete());
  }
  viewUser() {
    this.store.dispatch(new viewUser());
  }
}
