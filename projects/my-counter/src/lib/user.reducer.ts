import { Action } from "@ngrx/store";
import { ActionType } from "./user.actions";

export const users = [
  {
    name: "Super-Admin",
    age: 36,
    rights: ["admin"]
  }
];
export const user = {
  name: "Admin",
  age: 36,
  rights: ["admin", "editor", "contributor"]
};

export function userReducer(state = users, action: Action) {
  switch (action.type) {
    case ActionType.addUser:
      state.push({
        name: "user",
        age: 22,
        rights: ["contributor"]
      });
      return state;
      break;
    case ActionType.UpdateUser:
      state.map(x => {
        if (x.age > 22) {
          return (x.age = 18);
        }
      });
      return state;
      break;

    case ActionType.NewUser:
      state.push(user);
      return state;
      break;

    case ActionType.Delete:
      state = [];
      return state;
      break;

    case ActionType.viewUser:
      return state;
      break;

    default:
      return state;
  }
}
