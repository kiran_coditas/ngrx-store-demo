import { Action, createAction } from "@ngrx/store";
// export const addUser = createAction("[Counter Component] addUser");
// export const UpdateUser = createAction("[Counter Component] UpdateUser");
// export const NewUser = createAction("[Counter Component] NewUser");
// export const Delete = createAction("[Counter Component] Delete");
// export const viewUser = createAction("[Counter Component] viewUser");

export enum ActionType {
  addUser = "[Counter Component] addUser",
  UpdateUser = "[Counter Component] UpdateUser",
  NewUser = "[Counter Component] NewUser",
  Delete = "[Counter Component] Delete",
  viewUser = "[Counter Component] View User"
}

export class addUser implements Action {
  readonly type = ActionType.addUser;
}

export class UpdateUser implements Action {
  readonly type = ActionType.UpdateUser;
}

export class NewUser implements Action {
  readonly type = ActionType.NewUser;
}
export class Delete implements Action {
  readonly type = ActionType.Delete;
}
export class viewUser implements Action {
  readonly type = ActionType.viewUser;
}
