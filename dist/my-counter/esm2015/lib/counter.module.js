/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { NgModule, Injector } from "@angular/core";
import { CommonModule } from "@angular/common";
import { MatButtonModule } from "@angular/material/button";
import { MatCardModule } from "@angular/material/card";
import { CounterComponent } from "./counter.component";
import { CounterIncrementComponent } from "./counter-increment/counter-increment.component";
import { CounterDecrementComponent } from "./counter-decrement/counter-decrement.component";
import { CounterResetComponent } from "./counter-reset/counter-reset.component";
import { StoreModule } from "@ngrx/store";
import { counterReducer } from "./counter.reducer";
import { userReducer } from "./user.reducer";
import { createCustomElement } from "@angular/elements";
import { MyltiplyComponent } from "./myltiply/myltiply.component";
import { UserProfileComponent } from "./user-profile/user-profile.component";
export class CounterModule {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
        /** @type {?} */
        const CounterElement = createCustomElement(CounterComponent, { injector });
        // Register the custom element with the browser.
        customElements.define("counter-element", CounterElement);
        /** @type {?} */
        const CounterIncrementElement = createCustomElement(CounterIncrementComponent, { injector });
        customElements.define("counter-increment", CounterIncrementElement);
        /** @type {?} */
        const CounterDecrementElement = createCustomElement(CounterDecrementComponent, { injector });
        customElements.define("counter-decrement", CounterDecrementElement);
        /** @type {?} */
        const CounterResetElement = createCustomElement(CounterResetComponent, {
            injector
        });
        customElements.define("counter-reset", CounterResetElement);
        /** @type {?} */
        const CounterMultiplyElement = createCustomElement(MyltiplyComponent, {
            injector
        });
        customElements.define("counter-multiply", CounterMultiplyElement);
        /** @type {?} */
        const UserProfile = createCustomElement(UserProfileComponent, {
            injector
        });
        customElements.define("user-profile", UserProfile);
    }
}
CounterModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CounterComponent,
                    CounterIncrementComponent,
                    CounterDecrementComponent,
                    CounterResetComponent,
                    MyltiplyComponent,
                    UserProfileComponent
                ],
                imports: [
                    CommonModule,
                    MatButtonModule,
                    MatCardModule,
                    StoreModule.forRoot({ count: counterReducer, count1: userReducer })
                    // count: counterReducer,
                ],
                entryComponents: [
                    CounterComponent,
                    CounterIncrementComponent,
                    CounterDecrementComponent,
                    CounterResetComponent,
                    MyltiplyComponent,
                    UserProfileComponent
                ]
            },] }
];
/** @nocollapse */
CounterModule.ctorParameters = () => [
    { type: Injector }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterModule.prototype.injector;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci5tb2R1bGUuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1jb3VudGVyLyIsInNvdXJjZXMiOlsibGliL2NvdW50ZXIubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7O0FBQUEsT0FBTyxFQUFFLFFBQVEsRUFBRSxRQUFRLEVBQUUsTUFBTSxlQUFlLENBQUM7QUFDbkQsT0FBTyxFQUFFLFlBQVksRUFBRSxNQUFNLGlCQUFpQixDQUFDO0FBQy9DLE9BQU8sRUFBRSxlQUFlLEVBQUUsTUFBTSwwQkFBMEIsQ0FBQztBQUMzRCxPQUFPLEVBQUUsYUFBYSxFQUFFLE1BQU0sd0JBQXdCLENBQUM7QUFDdkQsT0FBTyxFQUFFLGdCQUFnQixFQUFFLE1BQU0scUJBQXFCLENBQUM7QUFDdkQsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDNUYsT0FBTyxFQUFFLHlCQUF5QixFQUFFLE1BQU0saURBQWlELENBQUM7QUFDNUYsT0FBTyxFQUFFLHFCQUFxQixFQUFFLE1BQU0seUNBQXlDLENBQUM7QUFDaEYsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGFBQWEsQ0FBQztBQUMxQyxPQUFPLEVBQUUsY0FBYyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7QUFDbkQsT0FBTyxFQUFFLFdBQVcsRUFBRSxNQUFNLGdCQUFnQixDQUFDO0FBQzdDLE9BQU8sRUFBRSxtQkFBbUIsRUFBRSxNQUFNLG1CQUFtQixDQUFDO0FBQ3hELE9BQU8sRUFBRSxpQkFBaUIsRUFBRSxNQUFNLCtCQUErQixDQUFDO0FBQ2xFLE9BQU8sRUFBRSxvQkFBb0IsRUFBRSxNQUFNLHVDQUF1QyxDQUFDO0FBMkI3RSxNQUFNLE9BQU8sYUFBYTs7OztJQUN4QixZQUFvQixRQUFrQjtRQUFsQixhQUFRLEdBQVIsUUFBUSxDQUFVOztjQUM5QixjQUFjLEdBQUcsbUJBQW1CLENBQUMsZ0JBQWdCLEVBQUUsRUFBRSxRQUFRLEVBQUUsQ0FBQztRQUMxRSxnREFBZ0Q7UUFDaEQsY0FBYyxDQUFDLE1BQU0sQ0FBQyxpQkFBaUIsRUFBRSxjQUFjLENBQUMsQ0FBQzs7Y0FFbkQsdUJBQXVCLEdBQUcsbUJBQW1CLENBQ2pELHlCQUF5QixFQUN6QixFQUFFLFFBQVEsRUFBRSxDQUNiO1FBQ0QsY0FBYyxDQUFDLE1BQU0sQ0FBQyxtQkFBbUIsRUFBRSx1QkFBdUIsQ0FBQyxDQUFDOztjQUM5RCx1QkFBdUIsR0FBRyxtQkFBbUIsQ0FDakQseUJBQXlCLEVBQ3pCLEVBQUUsUUFBUSxFQUFFLENBQ2I7UUFDRCxjQUFjLENBQUMsTUFBTSxDQUFDLG1CQUFtQixFQUFFLHVCQUF1QixDQUFDLENBQUM7O2NBQzlELG1CQUFtQixHQUFHLG1CQUFtQixDQUFDLHFCQUFxQixFQUFFO1lBQ3JFLFFBQVE7U0FDVCxDQUFDO1FBQ0YsY0FBYyxDQUFDLE1BQU0sQ0FBQyxlQUFlLEVBQUUsbUJBQW1CLENBQUMsQ0FBQzs7Y0FDdEQsc0JBQXNCLEdBQUcsbUJBQW1CLENBQUMsaUJBQWlCLEVBQUU7WUFDcEUsUUFBUTtTQUNULENBQUM7UUFDRixjQUFjLENBQUMsTUFBTSxDQUFDLGtCQUFrQixFQUFFLHNCQUFzQixDQUFDLENBQUM7O2NBQzVELFdBQVcsR0FBRyxtQkFBbUIsQ0FBQyxvQkFBb0IsRUFBRTtZQUM1RCxRQUFRO1NBQ1QsQ0FBQztRQUNGLGNBQWMsQ0FBQyxNQUFNLENBQUMsY0FBYyxFQUFFLFdBQVcsQ0FBQyxDQUFDO0lBQ3JELENBQUM7OztZQXJERixRQUFRLFNBQUM7Z0JBQ1IsWUFBWSxFQUFFO29CQUNaLGdCQUFnQjtvQkFDaEIseUJBQXlCO29CQUN6Qix5QkFBeUI7b0JBQ3pCLHFCQUFxQjtvQkFDckIsaUJBQWlCO29CQUNqQixvQkFBb0I7aUJBQ3JCO2dCQUNELE9BQU8sRUFBRTtvQkFDUCxZQUFZO29CQUNaLGVBQWU7b0JBQ2YsYUFBYTtvQkFDYixXQUFXLENBQUMsT0FBTyxDQUFDLEVBQUUsS0FBSyxFQUFFLGNBQWMsRUFBRSxNQUFNLEVBQUUsV0FBVyxFQUFFLENBQUM7b0JBQ25FLHlCQUF5QjtpQkFDMUI7Z0JBQ0QsZUFBZSxFQUFFO29CQUNmLGdCQUFnQjtvQkFDaEIseUJBQXlCO29CQUN6Qix5QkFBeUI7b0JBQ3pCLHFCQUFxQjtvQkFDckIsaUJBQWlCO29CQUNqQixvQkFBb0I7aUJBQ3JCO2FBQ0Y7Ozs7WUF2Q2tCLFFBQVE7Ozs7Ozs7SUF5Q2IsaUNBQTBCIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgTmdNb2R1bGUsIEluamVjdG9yIH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IENvbW1vbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9jb21tb25cIjtcbmltcG9ydCB7IE1hdEJ1dHRvbk1vZHVsZSB9IGZyb20gXCJAYW5ndWxhci9tYXRlcmlhbC9idXR0b25cIjtcbmltcG9ydCB7IE1hdENhcmRNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvbWF0ZXJpYWwvY2FyZFwiO1xuaW1wb3J0IHsgQ291bnRlckNvbXBvbmVudCB9IGZyb20gXCIuL2NvdW50ZXIuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDb3VudGVySW5jcmVtZW50Q29tcG9uZW50IH0gZnJvbSBcIi4vY291bnRlci1pbmNyZW1lbnQvY291bnRlci1pbmNyZW1lbnQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDb3VudGVyRGVjcmVtZW50Q29tcG9uZW50IH0gZnJvbSBcIi4vY291bnRlci1kZWNyZW1lbnQvY291bnRlci1kZWNyZW1lbnQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBDb3VudGVyUmVzZXRDb21wb25lbnQgfSBmcm9tIFwiLi9jb3VudGVyLXJlc2V0L2NvdW50ZXItcmVzZXQuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTdG9yZU1vZHVsZSB9IGZyb20gXCJAbmdyeC9zdG9yZVwiO1xuaW1wb3J0IHsgY291bnRlclJlZHVjZXIgfSBmcm9tIFwiLi9jb3VudGVyLnJlZHVjZXJcIjtcbmltcG9ydCB7IHVzZXJSZWR1Y2VyIH0gZnJvbSBcIi4vdXNlci5yZWR1Y2VyXCI7XG5pbXBvcnQgeyBjcmVhdGVDdXN0b21FbGVtZW50IH0gZnJvbSBcIkBhbmd1bGFyL2VsZW1lbnRzXCI7XG5pbXBvcnQgeyBNeWx0aXBseUNvbXBvbmVudCB9IGZyb20gXCIuL215bHRpcGx5L215bHRpcGx5LmNvbXBvbmVudFwiO1xuaW1wb3J0IHsgVXNlclByb2ZpbGVDb21wb25lbnQgfSBmcm9tIFwiLi91c2VyLXByb2ZpbGUvdXNlci1wcm9maWxlLmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBDb3VudGVyQ29tcG9uZW50LFxuICAgIENvdW50ZXJJbmNyZW1lbnRDb21wb25lbnQsXG4gICAgQ291bnRlckRlY3JlbWVudENvbXBvbmVudCxcbiAgICBDb3VudGVyUmVzZXRDb21wb25lbnQsXG4gICAgTXlsdGlwbHlDb21wb25lbnQsXG4gICAgVXNlclByb2ZpbGVDb21wb25lbnRcbiAgXSxcbiAgaW1wb3J0czogW1xuICAgIENvbW1vbk1vZHVsZSxcbiAgICBNYXRCdXR0b25Nb2R1bGUsXG4gICAgTWF0Q2FyZE1vZHVsZSxcbiAgICBTdG9yZU1vZHVsZS5mb3JSb290KHsgY291bnQ6IGNvdW50ZXJSZWR1Y2VyLCBjb3VudDE6IHVzZXJSZWR1Y2VyIH0pXG4gICAgLy8gY291bnQ6IGNvdW50ZXJSZWR1Y2VyLFxuICBdLFxuICBlbnRyeUNvbXBvbmVudHM6IFtcbiAgICBDb3VudGVyQ29tcG9uZW50LFxuICAgIENvdW50ZXJJbmNyZW1lbnRDb21wb25lbnQsXG4gICAgQ291bnRlckRlY3JlbWVudENvbXBvbmVudCxcbiAgICBDb3VudGVyUmVzZXRDb21wb25lbnQsXG4gICAgTXlsdGlwbHlDb21wb25lbnQsXG4gICAgVXNlclByb2ZpbGVDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBDb3VudGVyTW9kdWxlIHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBpbmplY3RvcjogSW5qZWN0b3IpIHtcbiAgICBjb25zdCBDb3VudGVyRWxlbWVudCA9IGNyZWF0ZUN1c3RvbUVsZW1lbnQoQ291bnRlckNvbXBvbmVudCwgeyBpbmplY3RvciB9KTtcbiAgICAvLyBSZWdpc3RlciB0aGUgY3VzdG9tIGVsZW1lbnQgd2l0aCB0aGUgYnJvd3Nlci5cbiAgICBjdXN0b21FbGVtZW50cy5kZWZpbmUoXCJjb3VudGVyLWVsZW1lbnRcIiwgQ291bnRlckVsZW1lbnQpO1xuXG4gICAgY29uc3QgQ291bnRlckluY3JlbWVudEVsZW1lbnQgPSBjcmVhdGVDdXN0b21FbGVtZW50KFxuICAgICAgQ291bnRlckluY3JlbWVudENvbXBvbmVudCxcbiAgICAgIHsgaW5qZWN0b3IgfVxuICAgICk7XG4gICAgY3VzdG9tRWxlbWVudHMuZGVmaW5lKFwiY291bnRlci1pbmNyZW1lbnRcIiwgQ291bnRlckluY3JlbWVudEVsZW1lbnQpO1xuICAgIGNvbnN0IENvdW50ZXJEZWNyZW1lbnRFbGVtZW50ID0gY3JlYXRlQ3VzdG9tRWxlbWVudChcbiAgICAgIENvdW50ZXJEZWNyZW1lbnRDb21wb25lbnQsXG4gICAgICB7IGluamVjdG9yIH1cbiAgICApO1xuICAgIGN1c3RvbUVsZW1lbnRzLmRlZmluZShcImNvdW50ZXItZGVjcmVtZW50XCIsIENvdW50ZXJEZWNyZW1lbnRFbGVtZW50KTtcbiAgICBjb25zdCBDb3VudGVyUmVzZXRFbGVtZW50ID0gY3JlYXRlQ3VzdG9tRWxlbWVudChDb3VudGVyUmVzZXRDb21wb25lbnQsIHtcbiAgICAgIGluamVjdG9yXG4gICAgfSk7XG4gICAgY3VzdG9tRWxlbWVudHMuZGVmaW5lKFwiY291bnRlci1yZXNldFwiLCBDb3VudGVyUmVzZXRFbGVtZW50KTtcbiAgICBjb25zdCBDb3VudGVyTXVsdGlwbHlFbGVtZW50ID0gY3JlYXRlQ3VzdG9tRWxlbWVudChNeWx0aXBseUNvbXBvbmVudCwge1xuICAgICAgaW5qZWN0b3JcbiAgICB9KTtcbiAgICBjdXN0b21FbGVtZW50cy5kZWZpbmUoXCJjb3VudGVyLW11bHRpcGx5XCIsIENvdW50ZXJNdWx0aXBseUVsZW1lbnQpO1xuICAgIGNvbnN0IFVzZXJQcm9maWxlID0gY3JlYXRlQ3VzdG9tRWxlbWVudChVc2VyUHJvZmlsZUNvbXBvbmVudCwge1xuICAgICAgaW5qZWN0b3JcbiAgICB9KTtcbiAgICBjdXN0b21FbGVtZW50cy5kZWZpbmUoXCJ1c2VyLXByb2ZpbGVcIiwgVXNlclByb2ZpbGUpO1xuICB9XG59XG4iXX0=