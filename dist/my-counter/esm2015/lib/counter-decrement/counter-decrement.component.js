/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-decrement/counter-decrement.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Decrement } from '../counter.actions';
export class CounterDecrementComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    decrement() {
        this.store.dispatch(new Decrement());
    }
}
CounterDecrementComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter-decrement',
                template: "<button mat-raised-button color=\"accent\" (click)=\"decrement()\">\n  Decrement\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterDecrementComponent.ctorParameters = () => [
    { type: Store }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterDecrementComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci1kZWNyZW1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLWRlY3JlbWVudC9jb3VudGVyLWRlY3JlbWVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRWxELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBTy9DLE1BQU0sT0FBTyx5QkFBeUI7Ozs7SUFFcEMsWUFBb0IsS0FBK0I7UUFBL0IsVUFBSyxHQUFMLEtBQUssQ0FBMEI7SUFBSSxDQUFDOzs7O0lBRXhELFFBQVE7SUFDUixDQUFDOzs7O0lBRUQsU0FBUztRQUNQLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksU0FBUyxFQUFFLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7WUFkRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtnQkFDakMseUdBQWlEOzthQUVsRDs7OztZQVBRLEtBQUs7Ozs7Ozs7SUFVQSwwQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyIgIGltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbiAgaW1wb3J0IHsgU3RvcmUgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG4gIGltcG9ydCB7IERlY3JlbWVudCB9IGZyb20gJy4uL2NvdW50ZXIuYWN0aW9ucyc7XG5cbiAgQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6ICdhcHAtY291bnRlci1kZWNyZW1lbnQnLFxuICAgIHRlbXBsYXRlVXJsOiAnLi9jb3VudGVyLWRlY3JlbWVudC5jb21wb25lbnQuaHRtbCcsXG4gICAgc3R5bGVVcmxzOiBbJy4vY291bnRlci1kZWNyZW1lbnQuY29tcG9uZW50LmNzcyddXG4gIH0pXG4gIGV4cG9ydCBjbGFzcyBDb3VudGVyRGVjcmVtZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICAgIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RvcmU6IFN0b3JlPHsgY291bnQ6IG51bWJlciB9PikgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICB9XG5cbiAgICBkZWNyZW1lbnQoKSB7XG4gICAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKG5ldyBEZWNyZW1lbnQoKSk7XG4gICAgfVxuXG4gIH1cbiJdfQ==