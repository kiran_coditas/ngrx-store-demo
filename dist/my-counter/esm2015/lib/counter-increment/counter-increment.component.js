/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-increment/counter-increment.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Increment } from '../counter.actions';
export class CounterIncrementComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    increment() {
        this.store.dispatch(new Increment());
    }
}
CounterIncrementComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter-increment',
                template: "<button mat-raised-button color=\"primary\" (click)=\"increment()\">Increment</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterIncrementComponent.ctorParameters = () => [
    { type: Store }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterIncrementComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci1pbmNyZW1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLWluY3JlbWVudC9jb3VudGVyLWluY3JlbWVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxLQUFLLEVBQVUsTUFBTSxhQUFhLENBQUM7QUFDNUMsT0FBTyxFQUFFLFNBQVMsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBTzlDLE1BQU0sT0FBTyx5QkFBeUI7Ozs7SUFFcEMsWUFBb0IsS0FBK0I7UUFBL0IsVUFBSyxHQUFMLEtBQUssQ0FBMEI7SUFBSSxDQUFDOzs7O0lBRXhELFFBQVE7SUFDUixDQUFDOzs7O0lBRUQsU0FBUztRQUNQLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksU0FBUyxFQUFFLENBQUMsQ0FBQztJQUN2QyxDQUFDOzs7WUFkRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLHVCQUF1QjtnQkFDakMsb0dBQWlEOzthQUVsRDs7OztZQVBRLEtBQUs7Ozs7Ozs7SUFVQSwwQ0FBdUMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuaW1wb3J0IHsgU3RvcmUsIHNlbGVjdCB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IEluY3JlbWVudH0gZnJvbSAnLi4vY291bnRlci5hY3Rpb25zJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWNvdW50ZXItaW5jcmVtZW50JyxcbiAgdGVtcGxhdGVVcmw6ICcuL2NvdW50ZXItaW5jcmVtZW50LmNvbXBvbmVudC5odG1sJyxcbiAgc3R5bGVVcmxzOiBbJy4vY291bnRlci1pbmNyZW1lbnQuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvdW50ZXJJbmNyZW1lbnRDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgc3RvcmU6IFN0b3JlPHsgY291bnQ6IG51bWJlciB9PikgeyB9XG5cbiAgbmdPbkluaXQoKSB7XG4gIH1cblxuICBpbmNyZW1lbnQoKSB7XG4gICAgdGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgSW5jcmVtZW50KCkpO1xuICB9XG5cbn1cbiJdfQ==