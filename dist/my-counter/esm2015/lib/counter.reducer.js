/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { ActionTypes } from "./counter.actions";
/** @type {?} */
export const initialState = 0;
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
export function counterReducer(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.Increment:
            return state + 1;
        case ActionTypes.Decrement:
            return state > 0 ? state - 1 : state;
        case ActionTypes.Multiplycount:
            return state * 2;
        case ActionTypes.Reset:
            return 0;
        default:
            return state;
    }
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci5yZWR1Y2VyLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLnJlZHVjZXIudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFDQSxPQUFPLEVBQUUsV0FBVyxFQUFFLE1BQU0sbUJBQW1CLENBQUM7O0FBRWhELE1BQU0sT0FBTyxZQUFZLEdBQUcsQ0FBQzs7Ozs7O0FBRTdCLE1BQU0sVUFBVSxjQUFjLENBQUMsS0FBSyxHQUFHLFlBQVksRUFBRSxNQUFjO0lBQ2pFLFFBQVEsTUFBTSxDQUFDLElBQUksRUFBRTtRQUNuQixLQUFLLFdBQVcsQ0FBQyxTQUFTO1lBQ3hCLE9BQU8sS0FBSyxHQUFHLENBQUMsQ0FBQztRQUNuQixLQUFLLFdBQVcsQ0FBQyxTQUFTO1lBQ3hCLE9BQU8sS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxHQUFHLENBQUMsQ0FBQyxDQUFDLENBQUMsS0FBSyxDQUFDO1FBQ3ZDLEtBQUssV0FBVyxDQUFDLGFBQWE7WUFDNUIsT0FBTyxLQUFLLEdBQUcsQ0FBQyxDQUFDO1FBQ25CLEtBQUssV0FBVyxDQUFDLEtBQUs7WUFDcEIsT0FBTyxDQUFDLENBQUM7UUFFWDtZQUNFLE9BQU8sS0FBSyxDQUFDO0tBQ2hCO0FBQ0gsQ0FBQyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEFjdGlvbiB9IGZyb20gXCJAbmdyeC9zdG9yZVwiO1xuaW1wb3J0IHsgQWN0aW9uVHlwZXMgfSBmcm9tIFwiLi9jb3VudGVyLmFjdGlvbnNcIjtcblxuZXhwb3J0IGNvbnN0IGluaXRpYWxTdGF0ZSA9IDA7XG5cbmV4cG9ydCBmdW5jdGlvbiBjb3VudGVyUmVkdWNlcihzdGF0ZSA9IGluaXRpYWxTdGF0ZSwgYWN0aW9uOiBBY3Rpb24pIHtcbiAgc3dpdGNoIChhY3Rpb24udHlwZSkge1xuICAgIGNhc2UgQWN0aW9uVHlwZXMuSW5jcmVtZW50OlxuICAgICAgcmV0dXJuIHN0YXRlICsgMTtcbiAgICBjYXNlIEFjdGlvblR5cGVzLkRlY3JlbWVudDpcbiAgICAgIHJldHVybiBzdGF0ZSA+IDAgPyBzdGF0ZSAtIDEgOiBzdGF0ZTtcbiAgICBjYXNlIEFjdGlvblR5cGVzLk11bHRpcGx5Y291bnQ6XG4gICAgICByZXR1cm4gc3RhdGUgKiAyO1xuICAgIGNhc2UgQWN0aW9uVHlwZXMuUmVzZXQ6XG4gICAgICByZXR1cm4gMDtcblxuICAgIGRlZmF1bHQ6XG4gICAgICByZXR1cm4gc3RhdGU7XG4gIH1cbn1cbiJdfQ==