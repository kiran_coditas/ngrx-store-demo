/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ActionTypes = {
    Increment: "[Counter Component] Increment",
    Decrement: "[Counter Component] Decrement",
    Reset: "[Counter Component] Reset",
    Multiplycount: "[Counter Component] Multiply",
};
export { ActionTypes };
export class Increment {
    constructor() {
        this.type = ActionTypes.Increment;
    }
}
if (false) {
    /** @type {?} */
    Increment.prototype.type;
}
export class Decrement {
    constructor() {
        this.type = ActionTypes.Decrement;
    }
}
if (false) {
    /** @type {?} */
    Decrement.prototype.type;
}
export class Reset {
    constructor() {
        this.type = ActionTypes.Reset;
    }
}
if (false) {
    /** @type {?} */
    Reset.prototype.type;
}
export class Multiplycount {
    constructor() {
        this.type = ActionTypes.Multiplycount;
    }
}
if (false) {
    /** @type {?} */
    Multiplycount.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci5hY3Rpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBRUEsTUFBWSxXQUFXO0lBQ3JCLFNBQVMsaUNBQWtDO0lBQzNDLFNBQVMsaUNBQWtDO0lBQzNDLEtBQUssNkJBQThCO0lBQ25DLGFBQWEsZ0NBQWlDO0VBQy9DOztBQUVELE1BQU0sT0FBTyxTQUFTO0lBQXRCO1FBQ1csU0FBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUM7SUFDeEMsQ0FBQztDQUFBOzs7SUFEQyx5QkFBc0M7O0FBR3hDLE1BQU0sT0FBTyxTQUFTO0lBQXRCO1FBQ1csU0FBSSxHQUFHLFdBQVcsQ0FBQyxTQUFTLENBQUM7SUFDeEMsQ0FBQztDQUFBOzs7SUFEQyx5QkFBc0M7O0FBR3hDLE1BQU0sT0FBTyxLQUFLO0lBQWxCO1FBQ1csU0FBSSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQztDQUFBOzs7SUFEQyxxQkFBa0M7O0FBRXBDLE1BQU0sT0FBTyxhQUFhO0lBQTFCO1FBQ1csU0FBSSxHQUFHLFdBQVcsQ0FBQyxhQUFhLENBQUM7SUFDNUMsQ0FBQztDQUFBOzs7SUFEQyw2QkFBMEMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBBY3Rpb24gfSBmcm9tIFwiQG5ncngvc3RvcmVcIjtcblxuZXhwb3J0IGVudW0gQWN0aW9uVHlwZXMge1xuICBJbmNyZW1lbnQgPSBcIltDb3VudGVyIENvbXBvbmVudF0gSW5jcmVtZW50XCIsXG4gIERlY3JlbWVudCA9IFwiW0NvdW50ZXIgQ29tcG9uZW50XSBEZWNyZW1lbnRcIixcbiAgUmVzZXQgPSBcIltDb3VudGVyIENvbXBvbmVudF0gUmVzZXRcIixcbiAgTXVsdGlwbHljb3VudCA9IFwiW0NvdW50ZXIgQ29tcG9uZW50XSBNdWx0aXBseVwiXG59XG5cbmV4cG9ydCBjbGFzcyBJbmNyZW1lbnQgaW1wbGVtZW50cyBBY3Rpb24ge1xuICByZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuSW5jcmVtZW50O1xufVxuXG5leHBvcnQgY2xhc3MgRGVjcmVtZW50IGltcGxlbWVudHMgQWN0aW9uIHtcbiAgcmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLkRlY3JlbWVudDtcbn1cblxuZXhwb3J0IGNsYXNzIFJlc2V0IGltcGxlbWVudHMgQWN0aW9uIHtcbiAgcmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLlJlc2V0O1xufVxuZXhwb3J0IGNsYXNzIE11bHRpcGx5Y291bnQgaW1wbGVtZW50cyBBY3Rpb24ge1xuICByZWFkb25seSB0eXBlID0gQWN0aW9uVHlwZXMuTXVsdGlwbHljb3VudDtcbn1cbiJdfQ==