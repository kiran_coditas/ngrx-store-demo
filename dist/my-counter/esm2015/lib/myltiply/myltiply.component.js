/**
 * @fileoverview added by tsickle
 * Generated from: lib/myltiply/myltiply.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from "@angular/core";
import { Multiplycount } from "../counter.actions";
import { Store } from "@ngrx/store";
export class MyltiplyComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    Multiply() {
        this.store.dispatch(new Multiplycount());
    }
}
MyltiplyComponent.decorators = [
    { type: Component, args: [{
                selector: "lib-myltiply",
                template: "<button mat-raised-button color=\"primary\" (click)=\"Multiply()\">\n  Multiply BY 2\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
MyltiplyComponent.ctorParameters = () => [
    { type: Store }
];
if (false) {
    /**
     * @type {?}
     * @private
     */
    MyltiplyComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlsdGlwbHkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9teWx0aXBseS9teWx0aXBseS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBTXBDLE1BQU0sT0FBTyxpQkFBaUI7Ozs7SUFDNUIsWUFBb0IsS0FBK0I7UUFBL0IsVUFBSyxHQUFMLEtBQUssQ0FBMEI7SUFBRyxDQUFDOzs7O0lBRXZELFFBQVEsS0FBSSxDQUFDOzs7O0lBQ2IsUUFBUTtRQUNOLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLElBQUksYUFBYSxFQUFFLENBQUMsQ0FBQztJQUMzQyxDQUFDOzs7WUFYRixTQUFTLFNBQUM7Z0JBQ1QsUUFBUSxFQUFFLGNBQWM7Z0JBQ3hCLDZHQUF3Qzs7YUFFekM7Ozs7WUFMUSxLQUFLOzs7Ozs7O0lBT0Esa0NBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuaW1wb3J0IHsgTXVsdGlwbHljb3VudCB9IGZyb20gXCIuLi9jb3VudGVyLmFjdGlvbnNcIjtcbmltcG9ydCB7IFN0b3JlIH0gZnJvbSBcIkBuZ3J4L3N0b3JlXCI7XG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6IFwibGliLW15bHRpcGx5XCIsXG4gIHRlbXBsYXRlVXJsOiBcIi4vbXlsdGlwbHkuY29tcG9uZW50Lmh0bWxcIixcbiAgc3R5bGVVcmxzOiBbXCIuL215bHRpcGx5LmNvbXBvbmVudC5jc3NcIl1cbn0pXG5leHBvcnQgY2xhc3MgTXlsdGlwbHlDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0b3JlOiBTdG9yZTx7IGNvdW50OiBudW1iZXIgfT4pIHt9XG5cbiAgbmdPbkluaXQoKSB7fVxuICBNdWx0aXBseSgpIHtcbiAgICB0aGlzLnN0b3JlLmRpc3BhdGNoKG5ldyBNdWx0aXBseWNvdW50KCkpO1xuICB9XG59XG4iXX0=