import { Action } from "@ngrx/store";
export declare const initialState = 0;
export declare function counterReducer(state: number, action: Action): number;
