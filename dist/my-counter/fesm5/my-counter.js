import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { createCustomElement } from '@angular/elements';
import { Component, NgModule, Injector } from '@angular/core';
import { Store, select, StoreModule } from '@ngrx/store';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var ActionTypes = {
    Increment: "[Counter Component] Increment",
    Decrement: "[Counter Component] Decrement",
    Reset: "[Counter Component] Reset",
    Multiplycount: "[Counter Component] Multiply",
};
var Increment = /** @class */ (function () {
    function Increment() {
        this.type = ActionTypes.Increment;
    }
    return Increment;
}());
var Decrement = /** @class */ (function () {
    function Decrement() {
        this.type = ActionTypes.Decrement;
    }
    return Decrement;
}());
var Reset = /** @class */ (function () {
    function Reset() {
        this.type = ActionTypes.Reset;
    }
    return Reset;
}());
var Multiplycount = /** @class */ (function () {
    function Multiplycount() {
        this.type = ActionTypes.Multiplycount;
    }
    return Multiplycount;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CounterComponent = /** @class */ (function () {
    function CounterComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.count$ = this.store.pipe(select('count'));
    };
    CounterComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter',
                    template: "<div>Current Count: {{ count$ | async }}</div>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-increment/counter-increment.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CounterIncrementComponent = /** @class */ (function () {
    function CounterIncrementComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterIncrementComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterIncrementComponent.prototype.increment = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Increment());
    };
    CounterIncrementComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-increment',
                    template: "<button mat-raised-button color=\"primary\" (click)=\"increment()\">Increment</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterIncrementComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterIncrementComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-decrement/counter-decrement.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CounterDecrementComponent = /** @class */ (function () {
    function CounterDecrementComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterDecrementComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterDecrementComponent.prototype.decrement = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Decrement());
    };
    CounterDecrementComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-decrement',
                    template: "<button mat-raised-button color=\"accent\" (click)=\"decrement()\">\n  Decrement\n</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterDecrementComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterDecrementComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-reset/counter-reset.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CounterResetComponent = /** @class */ (function () {
    function CounterResetComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterResetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterResetComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Reset());
    };
    CounterResetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-reset',
                    template: "<button mat-raised-button color=\"warn\" (click)=\"reset()\">Reset Counter</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterResetComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterResetComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var initialState = 0;
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function counterReducer(state, action) {
    if (state === void 0) { state = initialState; }
    switch (action.type) {
        case ActionTypes.Increment:
            return state + 1;
        case ActionTypes.Decrement:
            return state > 0 ? state - 1 : state;
        case ActionTypes.Multiplycount:
            return state * 2;
        case ActionTypes.Reset:
            return 0;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var ActionType = {
    addUser: "[Counter Component] addUser",
    UpdateUser: "[Counter Component] UpdateUser",
    NewUser: "[Counter Component] NewUser",
    Delete: "[Counter Component] Delete",
    viewUser: "[Counter Component] View User",
};
var addUser = /** @class */ (function () {
    function addUser() {
        this.type = ActionType.addUser;
    }
    return addUser;
}());
var UpdateUser = /** @class */ (function () {
    function UpdateUser() {
        this.type = ActionType.UpdateUser;
    }
    return UpdateUser;
}());
var NewUser = /** @class */ (function () {
    function NewUser() {
        this.type = ActionType.NewUser;
    }
    return NewUser;
}());
var Delete = /** @class */ (function () {
    function Delete() {
        this.type = ActionType.Delete;
    }
    return Delete;
}());
var viewUser = /** @class */ (function () {
    function viewUser() {
        this.type = ActionType.viewUser;
    }
    return viewUser;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
var users = [
    {
        name: "Super-Admin",
        age: 36,
        rights: ["admin"]
    }
];
/** @type {?} */
var user = {
    name: "Admin",
    age: 36,
    rights: ["admin", "editor", "contributor"]
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function userReducer(state, action) {
    if (state === void 0) { state = users; }
    switch (action.type) {
        case ActionType.addUser:
            state.push({
                name: "user",
                age: 22,
                rights: ["contributor"]
            });
            return state;
            break;
        case ActionType.UpdateUser:
            state.map((/**
             * @param {?} x
             * @return {?}
             */
            function (x) {
                if (x.age > 22) {
                    return (x.age = 18);
                }
            }));
            return state;
            break;
        case ActionType.NewUser:
            state.push(user);
            return state;
            break;
        case ActionType.Delete:
            state = [];
            return state;
            break;
        case ActionType.viewUser:
            return state;
            break;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/myltiply/myltiply.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var MyltiplyComponent = /** @class */ (function () {
    function MyltiplyComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    MyltiplyComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    MyltiplyComponent.prototype.Multiply = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Multiplycount());
    };
    MyltiplyComponent.decorators = [
        { type: Component, args: [{
                    selector: "lib-myltiply",
                    template: "<button mat-raised-button color=\"primary\" (click)=\"Multiply()\">\n  Multiply BY 2\n</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    MyltiplyComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return MyltiplyComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user-profile/user-profile.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var UserProfileComponent = /** @class */ (function () {
    function UserProfileComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
        this.count1$ = this.store.pipe(select("count1"));
    };
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.addUser = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new addUser());
    };
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.NewUser = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new NewUser());
    };
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.UpdateUser = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new UpdateUser());
    };
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.Delete = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Delete());
    };
    /**
     * @return {?}
     */
    UserProfileComponent.prototype.viewUser = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new viewUser());
    };
    UserProfileComponent.decorators = [
        { type: Component, args: [{
                    selector: "lib-user-profile",
                    template: "<button mat-raised-button color=\"primary\" (click)=\"addUser()\">\n  Add User\n</button>\n<button mat-raised-button color=\"accent\" (click)=\"UpdateUser()\">\n  Update User\n</button>\n<button mat-raised-button color=\"primary\" (click)=\"NewUser()\">\n  New User\n</button>\n<button mat-raised-button color=\"accent\" (click)=\"Delete()\">Delete</button>\n<button mat-raised-button color=\"primary\" (click)=\"viewUser()\">view</button>\n\n<mat-card *ngFor=\"let count of count1$ | async\">\n  <ul>\n    <li><strong>Name:</strong>{{ count.name }}</li>\n    <li><strong>age:</strong>{{ count.age }}</li>\n    <li><strong>rights:</strong>{{ count.rights }}</li>\n  </ul>\n</mat-card>\n",
                    styles: ["button{margin:10px}"]
                }] }
    ];
    /** @nocollapse */
    UserProfileComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return UserProfileComponent;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
var CounterModule = /** @class */ (function () {
    function CounterModule(injector) {
        this.injector = injector;
        /** @type {?} */
        var CounterElement = createCustomElement(CounterComponent, { injector: injector });
        // Register the custom element with the browser.
        customElements.define("counter-element", CounterElement);
        /** @type {?} */
        var CounterIncrementElement = createCustomElement(CounterIncrementComponent, { injector: injector });
        customElements.define("counter-increment", CounterIncrementElement);
        /** @type {?} */
        var CounterDecrementElement = createCustomElement(CounterDecrementComponent, { injector: injector });
        customElements.define("counter-decrement", CounterDecrementElement);
        /** @type {?} */
        var CounterResetElement = createCustomElement(CounterResetComponent, {
            injector: injector
        });
        customElements.define("counter-reset", CounterResetElement);
        /** @type {?} */
        var CounterMultiplyElement = createCustomElement(MyltiplyComponent, {
            injector: injector
        });
        customElements.define("counter-multiply", CounterMultiplyElement);
        /** @type {?} */
        var UserProfile = createCustomElement(UserProfileComponent, {
            injector: injector
        });
        customElements.define("user-profile", UserProfile);
    }
    CounterModule.decorators = [
        { type: NgModule, args: [{
                    declarations: [
                        CounterComponent,
                        CounterIncrementComponent,
                        CounterDecrementComponent,
                        CounterResetComponent,
                        MyltiplyComponent,
                        UserProfileComponent
                    ],
                    imports: [
                        CommonModule,
                        MatButtonModule,
                        MatCardModule,
                        StoreModule.forRoot({ count: counterReducer, count1: userReducer })
                        // count: counterReducer,
                    ],
                    entryComponents: [
                        CounterComponent,
                        CounterIncrementComponent,
                        CounterDecrementComponent,
                        CounterResetComponent,
                        MyltiplyComponent,
                        UserProfileComponent
                    ]
                },] }
    ];
    /** @nocollapse */
    CounterModule.ctorParameters = function () { return [
        { type: Injector }
    ]; };
    return CounterModule;
}());

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: my-counter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ActionTypes, Increment, Decrement, Reset, Multiplycount, CounterComponent, CounterModule, counterReducer, initialState, ActionType, addUser, UpdateUser, NewUser, Delete, viewUser, userReducer, users, user, UserProfileComponent, CounterDecrementComponent, CounterIncrementComponent, CounterResetComponent, MyltiplyComponent };

//# sourceMappingURL=my-counter.js.map