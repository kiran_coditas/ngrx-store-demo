/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
var ActionTypes = {
    Increment: "[Counter Component] Increment",
    Decrement: "[Counter Component] Decrement",
    Reset: "[Counter Component] Reset",
    Multiplycount: "[Counter Component] Multiply",
};
export { ActionTypes };
var Increment = /** @class */ (function () {
    function Increment() {
        this.type = ActionTypes.Increment;
    }
    return Increment;
}());
export { Increment };
if (false) {
    /** @type {?} */
    Increment.prototype.type;
}
var Decrement = /** @class */ (function () {
    function Decrement() {
        this.type = ActionTypes.Decrement;
    }
    return Decrement;
}());
export { Decrement };
if (false) {
    /** @type {?} */
    Decrement.prototype.type;
}
var Reset = /** @class */ (function () {
    function Reset() {
        this.type = ActionTypes.Reset;
    }
    return Reset;
}());
export { Reset };
if (false) {
    /** @type {?} */
    Reset.prototype.type;
}
var Multiplycount = /** @class */ (function () {
    function Multiplycount() {
        this.type = ActionTypes.Multiplycount;
    }
    return Multiplycount;
}());
export { Multiplycount };
if (false) {
    /** @type {?} */
    Multiplycount.prototype.type;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci5hY3Rpb25zLmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLmFjdGlvbnMudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7O0FBRUEsSUFBWSxXQUFXO0lBQ3JCLFNBQVMsaUNBQWtDO0lBQzNDLFNBQVMsaUNBQWtDO0lBQzNDLEtBQUssNkJBQThCO0lBQ25DLGFBQWEsZ0NBQWlDO0VBQy9DOztBQUVEO0lBQUE7UUFDVyxTQUFJLEdBQUcsV0FBVyxDQUFDLFNBQVMsQ0FBQztJQUN4QyxDQUFDO0lBQUQsZ0JBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQzs7OztJQURDLHlCQUFzQzs7QUFHeEM7SUFBQTtRQUNXLFNBQUksR0FBRyxXQUFXLENBQUMsU0FBUyxDQUFDO0lBQ3hDLENBQUM7SUFBRCxnQkFBQztBQUFELENBQUMsQUFGRCxJQUVDOzs7O0lBREMseUJBQXNDOztBQUd4QztJQUFBO1FBQ1csU0FBSSxHQUFHLFdBQVcsQ0FBQyxLQUFLLENBQUM7SUFDcEMsQ0FBQztJQUFELFlBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQzs7OztJQURDLHFCQUFrQzs7QUFFcEM7SUFBQTtRQUNXLFNBQUksR0FBRyxXQUFXLENBQUMsYUFBYSxDQUFDO0lBQzVDLENBQUM7SUFBRCxvQkFBQztBQUFELENBQUMsQUFGRCxJQUVDOzs7O0lBREMsNkJBQTBDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQWN0aW9uIH0gZnJvbSBcIkBuZ3J4L3N0b3JlXCI7XG5cbmV4cG9ydCBlbnVtIEFjdGlvblR5cGVzIHtcbiAgSW5jcmVtZW50ID0gXCJbQ291bnRlciBDb21wb25lbnRdIEluY3JlbWVudFwiLFxuICBEZWNyZW1lbnQgPSBcIltDb3VudGVyIENvbXBvbmVudF0gRGVjcmVtZW50XCIsXG4gIFJlc2V0ID0gXCJbQ291bnRlciBDb21wb25lbnRdIFJlc2V0XCIsXG4gIE11bHRpcGx5Y291bnQgPSBcIltDb3VudGVyIENvbXBvbmVudF0gTXVsdGlwbHlcIlxufVxuXG5leHBvcnQgY2xhc3MgSW5jcmVtZW50IGltcGxlbWVudHMgQWN0aW9uIHtcbiAgcmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLkluY3JlbWVudDtcbn1cblxuZXhwb3J0IGNsYXNzIERlY3JlbWVudCBpbXBsZW1lbnRzIEFjdGlvbiB7XG4gIHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5EZWNyZW1lbnQ7XG59XG5cbmV4cG9ydCBjbGFzcyBSZXNldCBpbXBsZW1lbnRzIEFjdGlvbiB7XG4gIHJlYWRvbmx5IHR5cGUgPSBBY3Rpb25UeXBlcy5SZXNldDtcbn1cbmV4cG9ydCBjbGFzcyBNdWx0aXBseWNvdW50IGltcGxlbWVudHMgQWN0aW9uIHtcbiAgcmVhZG9ubHkgdHlwZSA9IEFjdGlvblR5cGVzLk11bHRpcGx5Y291bnQ7XG59XG4iXX0=