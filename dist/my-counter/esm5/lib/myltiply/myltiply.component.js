/**
 * @fileoverview added by tsickle
 * Generated from: lib/myltiply/myltiply.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from "@angular/core";
import { Multiplycount } from "../counter.actions";
import { Store } from "@ngrx/store";
var MyltiplyComponent = /** @class */ (function () {
    function MyltiplyComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    MyltiplyComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () { };
    /**
     * @return {?}
     */
    MyltiplyComponent.prototype.Multiply = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Multiplycount());
    };
    MyltiplyComponent.decorators = [
        { type: Component, args: [{
                    selector: "lib-myltiply",
                    template: "<button mat-raised-button color=\"primary\" (click)=\"Multiply()\">\n  Multiply BY 2\n</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    MyltiplyComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return MyltiplyComponent;
}());
export { MyltiplyComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    MyltiplyComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoibXlsdGlwbHkuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9teWx0aXBseS9teWx0aXBseS5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxhQUFhLEVBQUUsTUFBTSxvQkFBb0IsQ0FBQztBQUNuRCxPQUFPLEVBQUUsS0FBSyxFQUFFLE1BQU0sYUFBYSxDQUFDO0FBQ3BDO0lBTUUsMkJBQW9CLEtBQStCO1FBQS9CLFVBQUssR0FBTCxLQUFLLENBQTBCO0lBQUcsQ0FBQzs7OztJQUV2RCxvQ0FBUTs7O0lBQVIsY0FBWSxDQUFDOzs7O0lBQ2Isb0NBQVE7OztJQUFSO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxhQUFhLEVBQUUsQ0FBQyxDQUFDO0lBQzNDLENBQUM7O2dCQVhGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsY0FBYztvQkFDeEIsNkdBQXdDOztpQkFFekM7Ozs7Z0JBTFEsS0FBSzs7SUFhZCx3QkFBQztDQUFBLEFBWkQsSUFZQztTQVBZLGlCQUFpQjs7Ozs7O0lBQ2hCLGtDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IE11bHRpcGx5Y291bnQgfSBmcm9tIFwiLi4vY291bnRlci5hY3Rpb25zXCI7XG5pbXBvcnQgeyBTdG9yZSB9IGZyb20gXCJAbmdyeC9zdG9yZVwiO1xuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiBcImxpYi1teWx0aXBseVwiLFxuICB0ZW1wbGF0ZVVybDogXCIuL215bHRpcGx5LmNvbXBvbmVudC5odG1sXCIsXG4gIHN0eWxlVXJsczogW1wiLi9teWx0aXBseS5jb21wb25lbnQuY3NzXCJdXG59KVxuZXhwb3J0IGNsYXNzIE15bHRpcGx5Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzdG9yZTogU3RvcmU8eyBjb3VudDogbnVtYmVyIH0+KSB7fVxuXG4gIG5nT25Jbml0KCkge31cbiAgTXVsdGlwbHkoKSB7XG4gICAgdGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgTXVsdGlwbHljb3VudCgpKTtcbiAgfVxufVxuIl19