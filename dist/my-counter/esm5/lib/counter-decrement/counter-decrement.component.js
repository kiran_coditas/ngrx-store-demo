/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-decrement/counter-decrement.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Decrement } from '../counter.actions';
var CounterDecrementComponent = /** @class */ (function () {
    function CounterDecrementComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterDecrementComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterDecrementComponent.prototype.decrement = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Decrement());
    };
    CounterDecrementComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-decrement',
                    template: "<button mat-raised-button color=\"accent\" (click)=\"decrement()\">\n  Decrement\n</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterDecrementComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterDecrementComponent;
}());
export { CounterDecrementComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterDecrementComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci1kZWNyZW1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLWRlY3JlbWVudC9jb3VudGVyLWRlY3JlbWVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBRSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBRWxELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxFQUFFLFNBQVMsRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRS9DO0lBT0UsbUNBQW9CLEtBQStCO1FBQS9CLFVBQUssR0FBTCxLQUFLLENBQTBCO0lBQUksQ0FBQzs7OztJQUV4RCw0Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQsNkNBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyx5R0FBaUQ7O2lCQUVsRDs7OztnQkFQUSxLQUFLOztJQW1CZCxnQ0FBQztDQUFBLEFBaEJELElBZ0JDO1NBWFkseUJBQXlCOzs7Ozs7SUFFeEIsMENBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiICBpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG4gIGltcG9ydCB7IFN0b3JlIH0gZnJvbSAnQG5ncngvc3RvcmUnO1xuICBpbXBvcnQgeyBEZWNyZW1lbnQgfSBmcm9tICcuLi9jb3VudGVyLmFjdGlvbnMnO1xuXG4gIEBDb21wb25lbnQoe1xuICAgIHNlbGVjdG9yOiAnYXBwLWNvdW50ZXItZGVjcmVtZW50JyxcbiAgICB0ZW1wbGF0ZVVybDogJy4vY291bnRlci1kZWNyZW1lbnQuY29tcG9uZW50Lmh0bWwnLFxuICAgIHN0eWxlVXJsczogWycuL2NvdW50ZXItZGVjcmVtZW50LmNvbXBvbmVudC5jc3MnXVxuICB9KVxuICBleHBvcnQgY2xhc3MgQ291bnRlckRlY3JlbWVudENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0b3JlOiBTdG9yZTx7IGNvdW50OiBudW1iZXIgfT4pIHsgfVxuXG4gICAgbmdPbkluaXQoKSB7XG4gICAgfVxuXG4gICAgZGVjcmVtZW50KCkge1xuICAgICAgdGhpcy5zdG9yZS5kaXNwYXRjaChuZXcgRGVjcmVtZW50KCkpO1xuICAgIH1cblxuICB9XG4iXX0=