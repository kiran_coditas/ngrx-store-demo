/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-increment/counter-increment.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Increment } from '../counter.actions';
var CounterIncrementComponent = /** @class */ (function () {
    function CounterIncrementComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterIncrementComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterIncrementComponent.prototype.increment = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Increment());
    };
    CounterIncrementComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-increment',
                    template: "<button mat-raised-button color=\"primary\" (click)=\"increment()\">Increment</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterIncrementComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterIncrementComponent;
}());
export { CounterIncrementComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterIncrementComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci1pbmNyZW1lbnQuY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6Im5nOi8vbXktY291bnRlci8iLCJzb3VyY2VzIjpbImxpYi9jb3VudGVyLWluY3JlbWVudC9jb3VudGVyLWluY3JlbWVudC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxLQUFLLEVBQVUsTUFBTSxhQUFhLENBQUM7QUFDNUMsT0FBTyxFQUFFLFNBQVMsRUFBQyxNQUFNLG9CQUFvQixDQUFDO0FBRTlDO0lBT0UsbUNBQW9CLEtBQStCO1FBQS9CLFVBQUssR0FBTCxLQUFLLENBQTBCO0lBQUksQ0FBQzs7OztJQUV4RCw0Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQsNkNBQVM7OztJQUFUO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxTQUFTLEVBQUUsQ0FBQyxDQUFDO0lBQ3ZDLENBQUM7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsdUJBQXVCO29CQUNqQyxvR0FBaUQ7O2lCQUVsRDs7OztnQkFQUSxLQUFLOztJQW1CZCxnQ0FBQztDQUFBLEFBaEJELElBZ0JDO1NBWFkseUJBQXlCOzs7Ozs7SUFFeEIsMENBQXVDIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IFN0b3JlLCBzZWxlY3QgfSBmcm9tICdAbmdyeC9zdG9yZSc7XG5pbXBvcnQgeyBJbmNyZW1lbnR9IGZyb20gJy4uL2NvdW50ZXIuYWN0aW9ucyc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1jb3VudGVyLWluY3JlbWVudCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9jb3VudGVyLWluY3JlbWVudC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvdW50ZXItaW5jcmVtZW50LmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBDb3VudGVySW5jcmVtZW50Q29tcG9uZW50IGltcGxlbWVudHMgT25Jbml0IHtcblxuICBjb25zdHJ1Y3Rvcihwcml2YXRlIHN0b3JlOiBTdG9yZTx7IGNvdW50OiBudW1iZXIgfT4pIHsgfVxuXG4gIG5nT25Jbml0KCkge1xuICB9XG5cbiAgaW5jcmVtZW50KCkge1xuICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IEluY3JlbWVudCgpKTtcbiAgfVxuXG59XG4iXX0=