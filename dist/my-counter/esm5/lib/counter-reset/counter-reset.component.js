/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-reset/counter-reset.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
import { Component } from '@angular/core';
import { Store } from '@ngrx/store';
import { Reset } from '../counter.actions';
var CounterResetComponent = /** @class */ (function () {
    function CounterResetComponent(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    CounterResetComponent.prototype.ngOnInit = /**
     * @return {?}
     */
    function () {
    };
    /**
     * @return {?}
     */
    CounterResetComponent.prototype.reset = /**
     * @return {?}
     */
    function () {
        this.store.dispatch(new Reset());
    };
    CounterResetComponent.decorators = [
        { type: Component, args: [{
                    selector: 'app-counter-reset',
                    template: "<button mat-raised-button color=\"warn\" (click)=\"reset()\">Reset Counter</button>\n",
                    styles: [""]
                }] }
    ];
    /** @nocollapse */
    CounterResetComponent.ctorParameters = function () { return [
        { type: Store }
    ]; };
    return CounterResetComponent;
}());
export { CounterResetComponent };
if (false) {
    /**
     * @type {?}
     * @private
     */
    CounterResetComponent.prototype.store;
}
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiY291bnRlci1yZXNldC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290Ijoibmc6Ly9teS1jb3VudGVyLyIsInNvdXJjZXMiOlsibGliL2NvdW50ZXItcmVzZXQvY291bnRlci1yZXNldC5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6Ijs7Ozs7QUFBQSxPQUFPLEVBQUUsU0FBUyxFQUFVLE1BQU0sZUFBZSxDQUFDO0FBQ2xELE9BQU8sRUFBRSxLQUFLLEVBQUUsTUFBTSxhQUFhLENBQUM7QUFDcEMsT0FBTyxFQUFFLEtBQUssRUFBRSxNQUFNLG9CQUFvQixDQUFDO0FBRTNDO0lBT0UsK0JBQW9CLEtBQStCO1FBQS9CLFVBQUssR0FBTCxLQUFLLENBQTBCO0lBQUksQ0FBQzs7OztJQUV4RCx3Q0FBUTs7O0lBQVI7SUFDQSxDQUFDOzs7O0lBRUQscUNBQUs7OztJQUFMO1FBQ0UsSUFBSSxDQUFDLEtBQUssQ0FBQyxRQUFRLENBQUMsSUFBSSxLQUFLLEVBQUUsQ0FBQyxDQUFDO0lBQ25DLENBQUM7O2dCQWRGLFNBQVMsU0FBQztvQkFDVCxRQUFRLEVBQUUsbUJBQW1CO29CQUM3QixpR0FBNkM7O2lCQUU5Qzs7OztnQkFQUSxLQUFLOztJQWtCZCw0QkFBQztDQUFBLEFBZkQsSUFlQztTQVZZLHFCQUFxQjs7Ozs7O0lBRXBCLHNDQUF1QyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBTdG9yZSB9IGZyb20gJ0BuZ3J4L3N0b3JlJztcbmltcG9ydCB7IFJlc2V0IH0gZnJvbSAnLi4vY291bnRlci5hY3Rpb25zJztcblxuQENvbXBvbmVudCh7XG4gIHNlbGVjdG9yOiAnYXBwLWNvdW50ZXItcmVzZXQnLFxuICB0ZW1wbGF0ZVVybDogJy4vY291bnRlci1yZXNldC5jb21wb25lbnQuaHRtbCcsXG4gIHN0eWxlVXJsczogWycuL2NvdW50ZXItcmVzZXQuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIENvdW50ZXJSZXNldENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBzdG9yZTogU3RvcmU8eyBjb3VudDogbnVtYmVyIH0+KSB7IH1cblxuICBuZ09uSW5pdCgpIHtcbiAgfVxuXG4gIHJlc2V0KCkge1xuICAgIHRoaXMuc3RvcmUuZGlzcGF0Y2gobmV3IFJlc2V0KCkpO1xuICB9XG59XG4iXX0=