import { CommonModule } from '@angular/common';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { createCustomElement } from '@angular/elements';
import { Component, NgModule, Injector } from '@angular/core';
import { Store, select, StoreModule } from '@ngrx/store';

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ActionTypes = {
    Increment: "[Counter Component] Increment",
    Decrement: "[Counter Component] Decrement",
    Reset: "[Counter Component] Reset",
    Multiplycount: "[Counter Component] Multiply",
};
class Increment {
    constructor() {
        this.type = ActionTypes.Increment;
    }
}
class Decrement {
    constructor() {
        this.type = ActionTypes.Decrement;
    }
}
class Reset {
    constructor() {
        this.type = ActionTypes.Reset;
    }
}
class Multiplycount {
    constructor() {
        this.type = ActionTypes.Multiplycount;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CounterComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.count$ = this.store.pipe(select('count'));
    }
}
CounterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter',
                template: "<div>Current Count: {{ count$ | async }}</div>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-increment/counter-increment.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CounterIncrementComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    increment() {
        this.store.dispatch(new Increment());
    }
}
CounterIncrementComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter-increment',
                template: "<button mat-raised-button color=\"primary\" (click)=\"increment()\">Increment</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterIncrementComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-decrement/counter-decrement.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CounterDecrementComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    decrement() {
        this.store.dispatch(new Decrement());
    }
}
CounterDecrementComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter-decrement',
                template: "<button mat-raised-button color=\"accent\" (click)=\"decrement()\">\n  Decrement\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterDecrementComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter-reset/counter-reset.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CounterResetComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
    }
    /**
     * @return {?}
     */
    reset() {
        this.store.dispatch(new Reset());
    }
}
CounterResetComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter-reset',
                template: "<button mat-raised-button color=\"warn\" (click)=\"reset()\">Reset Counter</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
CounterResetComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const initialState = 0;
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function counterReducer(state = initialState, action) {
    switch (action.type) {
        case ActionTypes.Increment:
            return state + 1;
        case ActionTypes.Decrement:
            return state > 0 ? state - 1 : state;
        case ActionTypes.Multiplycount:
            return state * 2;
        case ActionTypes.Reset:
            return 0;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user.actions.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @enum {string} */
const ActionType = {
    addUser: "[Counter Component] addUser",
    UpdateUser: "[Counter Component] UpdateUser",
    NewUser: "[Counter Component] NewUser",
    Delete: "[Counter Component] Delete",
    viewUser: "[Counter Component] View User",
};
class addUser {
    constructor() {
        this.type = ActionType.addUser;
    }
}
class UpdateUser {
    constructor() {
        this.type = ActionType.UpdateUser;
    }
}
class NewUser {
    constructor() {
        this.type = ActionType.NewUser;
    }
}
class Delete {
    constructor() {
        this.type = ActionType.Delete;
    }
}
class viewUser {
    constructor() {
        this.type = ActionType.viewUser;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user.reducer.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
/** @type {?} */
const users = [
    {
        name: "Super-Admin",
        age: 36,
        rights: ["admin"]
    }
];
/** @type {?} */
const user = {
    name: "Admin",
    age: 36,
    rights: ["admin", "editor", "contributor"]
};
/**
 * @param {?=} state
 * @param {?=} action
 * @return {?}
 */
function userReducer(state = users, action) {
    switch (action.type) {
        case ActionType.addUser:
            state.push({
                name: "user",
                age: 22,
                rights: ["contributor"]
            });
            return state;
            break;
        case ActionType.UpdateUser:
            state.map((/**
             * @param {?} x
             * @return {?}
             */
            x => {
                if (x.age > 22) {
                    return (x.age = 18);
                }
            }));
            return state;
            break;
        case ActionType.NewUser:
            state.push(user);
            return state;
            break;
        case ActionType.Delete:
            state = [];
            return state;
            break;
        case ActionType.viewUser:
            return state;
            break;
        default:
            return state;
    }
}

/**
 * @fileoverview added by tsickle
 * Generated from: lib/myltiply/myltiply.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class MyltiplyComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() { }
    /**
     * @return {?}
     */
    Multiply() {
        this.store.dispatch(new Multiplycount());
    }
}
MyltiplyComponent.decorators = [
    { type: Component, args: [{
                selector: "lib-myltiply",
                template: "<button mat-raised-button color=\"primary\" (click)=\"Multiply()\">\n  Multiply BY 2\n</button>\n",
                styles: [""]
            }] }
];
/** @nocollapse */
MyltiplyComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/user-profile/user-profile.component.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class UserProfileComponent {
    /**
     * @param {?} store
     */
    constructor(store) {
        this.store = store;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.count1$ = this.store.pipe(select("count1"));
    }
    /**
     * @return {?}
     */
    addUser() {
        this.store.dispatch(new addUser());
    }
    /**
     * @return {?}
     */
    NewUser() {
        this.store.dispatch(new NewUser());
    }
    /**
     * @return {?}
     */
    UpdateUser() {
        this.store.dispatch(new UpdateUser());
    }
    /**
     * @return {?}
     */
    Delete() {
        this.store.dispatch(new Delete());
    }
    /**
     * @return {?}
     */
    viewUser() {
        this.store.dispatch(new viewUser());
    }
}
UserProfileComponent.decorators = [
    { type: Component, args: [{
                selector: "lib-user-profile",
                template: "<button mat-raised-button color=\"primary\" (click)=\"addUser()\">\n  Add User\n</button>\n<button mat-raised-button color=\"accent\" (click)=\"UpdateUser()\">\n  Update User\n</button>\n<button mat-raised-button color=\"primary\" (click)=\"NewUser()\">\n  New User\n</button>\n<button mat-raised-button color=\"accent\" (click)=\"Delete()\">Delete</button>\n<button mat-raised-button color=\"primary\" (click)=\"viewUser()\">view</button>\n\n<mat-card *ngFor=\"let count of count1$ | async\">\n  <ul>\n    <li><strong>Name:</strong>{{ count.name }}</li>\n    <li><strong>age:</strong>{{ count.age }}</li>\n    <li><strong>rights:</strong>{{ count.rights }}</li>\n  </ul>\n</mat-card>\n",
                styles: ["button{margin:10px}"]
            }] }
];
/** @nocollapse */
UserProfileComponent.ctorParameters = () => [
    { type: Store }
];

/**
 * @fileoverview added by tsickle
 * Generated from: lib/counter.module.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */
class CounterModule {
    /**
     * @param {?} injector
     */
    constructor(injector) {
        this.injector = injector;
        /** @type {?} */
        const CounterElement = createCustomElement(CounterComponent, { injector });
        // Register the custom element with the browser.
        customElements.define("counter-element", CounterElement);
        /** @type {?} */
        const CounterIncrementElement = createCustomElement(CounterIncrementComponent, { injector });
        customElements.define("counter-increment", CounterIncrementElement);
        /** @type {?} */
        const CounterDecrementElement = createCustomElement(CounterDecrementComponent, { injector });
        customElements.define("counter-decrement", CounterDecrementElement);
        /** @type {?} */
        const CounterResetElement = createCustomElement(CounterResetComponent, {
            injector
        });
        customElements.define("counter-reset", CounterResetElement);
        /** @type {?} */
        const CounterMultiplyElement = createCustomElement(MyltiplyComponent, {
            injector
        });
        customElements.define("counter-multiply", CounterMultiplyElement);
        /** @type {?} */
        const UserProfile = createCustomElement(UserProfileComponent, {
            injector
        });
        customElements.define("user-profile", UserProfile);
    }
}
CounterModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    CounterComponent,
                    CounterIncrementComponent,
                    CounterDecrementComponent,
                    CounterResetComponent,
                    MyltiplyComponent,
                    UserProfileComponent
                ],
                imports: [
                    CommonModule,
                    MatButtonModule,
                    MatCardModule,
                    StoreModule.forRoot({ count: counterReducer, count1: userReducer })
                    // count: counterReducer,
                ],
                entryComponents: [
                    CounterComponent,
                    CounterIncrementComponent,
                    CounterDecrementComponent,
                    CounterResetComponent,
                    MyltiplyComponent,
                    UserProfileComponent
                ]
            },] }
];
/** @nocollapse */
CounterModule.ctorParameters = () => [
    { type: Injector }
];

/**
 * @fileoverview added by tsickle
 * Generated from: public-api.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

/**
 * @fileoverview added by tsickle
 * Generated from: my-counter.ts
 * @suppress {checkTypes,constantProperty,extraRequire,missingOverride,missingReturn,unusedPrivateMembers,uselessCode} checked by tsc
 */

export { ActionTypes, Increment, Decrement, Reset, Multiplycount, CounterComponent, CounterModule, counterReducer, initialState, ActionType, addUser, UpdateUser, NewUser, Delete, viewUser, userReducer, users, user, UserProfileComponent, CounterDecrementComponent, CounterIncrementComponent, CounterResetComponent, MyltiplyComponent };

//# sourceMappingURL=my-counter.js.map